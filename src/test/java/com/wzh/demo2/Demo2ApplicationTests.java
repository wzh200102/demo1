package com.wzh.demo2;

import com.wzh.demo2.dao.TestDao;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.Date;

@SpringBootTest
class Demo2ApplicationTests {

    @Autowired
    TestDao testDao;

    @Test
    void contextLoads() {
        testDao.insertTest(new com.wzh.demo2.pojo.Test("1","2",25));
    }
    @Test
    void test2() {
        System.out.println(testDao.selectTest(1));
    }
    @Autowired
    RedisTemplate redisTemplate;

    @Test
    void test1() {
        redisTemplate.opsForValue().set("test",new Date().toString());
    }
}
