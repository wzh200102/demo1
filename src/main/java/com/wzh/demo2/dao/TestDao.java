package com.wzh.demo2.dao;

import com.wzh.demo2.pojo.Test;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface TestDao {
    void insertTest(Test test);

    Test selectTest(Integer id);
}
