package com.wzh.demo2.pojo;

public class Test {
    public String test1;
    public String test2;
    public Integer number1;

    public String getTest1() {
        return test1;
    }

    public void setTest1(String test1) {
        this.test1 = test1;
    }

    public String getTest2() {
        return test2;
    }

    public void setTest2(String test2) {
        this.test2 = test2;
    }

    public Integer getNumber1() {
        return number1;
    }

    public void setNumber1(Integer number1) {
        this.number1 = number1;
    }

    public Test(String test1, String test2, Integer number1) {
        this.test1 = test1;
        this.test2 = test2;
        this.number1 = number1;
    }

    @Override
    public String toString() {
        return "Test{" +
                "test1='" + test1 + '\'' +
                ", test2='" + test2 + '\'' +
                ", number1=" + number1 +
                '}';
    }
}
